import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author nmessias
 */
public class Pratica31 {
    public static String nomeFormatado(List<String> nomes){
        String nome = nomes.get(nomes.size() - 1).substring(0, 1).toUpperCase() + nomes.get(nomes.size() - 1).substring(1).toLowerCase();
        nomes.remove(nomes.size() - 1); 
        nome += ",";
        for(String sobrenome : nomes){
            nome += " " + sobrenome.toUpperCase().charAt(0) + ".";
        }
                
        return nome;
    }
    
    public static long diferencaDias(Date d1, Date d2) {
        long dif = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(dif, TimeUnit.MILLISECONDS);
    }
    
    public static long diferencaMill(Date d1, Date d2) {
        return d2.getTime() - d1.getTime();
    }
        
    private static Date inicio = new Date();;
    private static String meuNome = "Nicolas Vilela Messias";
    private static GregorianCalendar dataNascimento = new GregorianCalendar(1998, 8, 11);
    private static Date fim;
    
    public static void main(String[] args) {
      
        System.out.println(meuNome.toUpperCase());
        
        List<String> nomeSeparado = new ArrayList<String>(Arrays.asList(meuNome.split(" ")));
        
        System.out.println(nomeFormatado(nomeSeparado));
        
        GregorianCalendar hoje = new GregorianCalendar();
        System.out.println(diferencaDias(dataNascimento.getTime(), hoje.getTime()));
        
        fim = new Date();
        System.out.println(diferencaMill(inicio, fim));
    } 
}
